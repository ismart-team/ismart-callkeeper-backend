<?php

namespace Ismart\Callkeeper;

class Callkeeper
{
	private $config = array(
		'unique' => 'company',
		'utc' => 'Europe/Moscow',
		"tool_name" => "Форма обратной связи",
	);

	public function __construct ($config)
	{
		$this->config = array_merge($this->config, $config);
	}

	public function send($data)
	{
		$query = $this->build_query($data);
		$protocol = 'https://';
		$url = $protocol . 'api.callkeeper.ru/makeCalls?' . $query;
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		curl_close($curl);

		if ($this->config["debug"]) {
      		var_dump($query);
			var_dump($response);
		}
	}

	private function is_empty($var)
	{
		$is_scalar = is_scalar($var);
		$has_length = strlen($var);
		return !($var || ($is_scalar && $has_length));
	}

	private function build_query($data)
	{
		$referrer = filter_var($data['referrer'], FILTER_SANITIZE_STRING);
		$search_engine = filter_var($data["searchEngine"], FILTER_SANITIZE_STRING);

		$query = array(
			"unique" => $this->config["unique"],
			"apiak" => $this->config["apiak"],
			"whash" => $this->config["whash"],
			"utc" => $this->config["utc"],
			"tool_name" => $this->config["tool_name"],
      		"ga_client_id" => filter_var($data["ga_client_id"], FILTER_SANITIZE_STRING)
		);

		if (array_key_exists("opening_hours", $this->config)) {
			$query["opening_hours"] = $this->config["opening_hours"];
		}

		if (array_key_exists("ya_client_id", $data)) {
			$query["ya_client_id"] = filter_var($data["ya_client_id"], FILTER_SANITIZE_STRING);
		}

		$query["ip_client"] = $this->get_ip();

		$query["calls"][0] = array(
			"client" => preg_replace('/[^0-9]/', '', filter_var($data["tel"], FILTER_SANITIZE_STRING)),
			"site" => $this->config["site_name"],
			"utm" => array(
				"entry_point" => $this->config["entry_point"],
			)
		);

        if (array_key_exists("text_to_manager", $this->config)) {
            $query["calls"][0]["text_to_manager"] = $this->config["text_to_manager"];
        }

        if (array_key_exists("manager", $this->config)) {
            $query["calls"][0]["manager"] = $this->config["manager"];
        }

		if (array_key_exists("utm_medium", $data)) {
			$query["calls"][0]["utm"]["utm_medium"] = filter_var($data['utm_medium'], FILTER_SANITIZE_STRING);
		}

		if (array_key_exists("utm_campaign", $data)) {
			$query["calls"][0]["utm"]["utm_campaign"] = filter_var($data['utm_campaign'], FILTER_SANITIZE_STRING);
		}

		if (array_key_exists("utm_content", $data)) {
			$query["calls"][0]["utm"]["utm_content"] = filter_var($data['utm_content'], FILTER_SANITIZE_STRING);
		}

		if (array_key_exists("utm_term", $data)) {
			$query["calls"][0]["utm"]["utm_term"] = filter_var($data['utm_term'], FILTER_SANITIZE_STRING);
		}

		if (array_key_exists("utm_source", $data)) {
			$query["calls"][0]["utm"]["utm_source"] = filter_var($data['utm_source'], FILTER_SANITIZE_STRING);
		} else if (!$this->is_empty($search_engine)) {
			$query["calls"][0]["utm"]["utm_type"] = "organic";
			$query["calls"][0]["utm"]["utm_source"] = $search_engine;
		} else if (!$this->is_empty($referrer)) {
			$query["calls"][0]["utm"]["utm_type"] = "referral";
			$query["calls"][0]["utm"]["utm_source"] = $referrer;
		} else {
			$query["calls"][0]["utm"]["utm_type"] = "typein";
		}

		if ($this->config["debug"]) {
			var_dump($query);
		}


		return http_build_query($query);
	}

	private function get_ip()
	{
		if (array_key_exists('HTTP_CLIENT_IP', $_SERVER) && !$this->is_empty($_SERVER['HTTP_CLIENT_IP'])) {
			return  $_SERVER['HTTP_CLIENT_IP'];
		} elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !$this->is_empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}
}
